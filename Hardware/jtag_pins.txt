Pin Name (GPIO No)      JTAG/SWD pin    AF Number   Is usable for external connection

1.  BCM 4 (GPCLK0)      JTAG TDI        ALT-5       Yes (Free pin)
2.  BCM 27              JTAG TMS        ALT-4       Yes (Free pin)
3.  BCM 22              JTAG TRST       ALT-4       Yes (Free pin)
4.  BCM 5               JTAG TDO        ALT-5       Yes (Free pin)
5.  BCM 6               JTAG RTICK      ALT-5       Yes (Free pin)
6.  BCM 13 (PWM1)       JTAG TCK        ALT-5       Yes (Free pin)
7.  BCM 26              JTAG TDI        ALT-4       Yes (Free pin)
8.  BCM 23              JTAG RTICK      ALT-4       Yes (Free pin)
9.  BCM 24              JTAG TDO        ALT-4       Yes (Free pin)
10. BCM 25              JTAG TCK        ALT-4       Yes (Free pin)
11. BCM 12 (PWM0)       JTAG TMS        ALT-5       Yes (Free pin)

