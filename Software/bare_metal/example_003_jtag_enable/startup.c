extern int main(int cpu_id, char **);
void _initialize_memory(void);
void infinite_loop(void);

void _start_code_cpu_0()
{
    _initialize_memory();
    main(0, 0);
}

void _start_code_cpu_1()
{
    infinite_loop();
}

void _start_code_cpu_2()
{
    infinite_loop();
}


void _start_code_cpu_3()
{
    infinite_loop();
}

void _initialize_memory(void)
{
    /* Only called from CPU0 code */
}

void infinite_loop(void)
{
    while (1);
}
