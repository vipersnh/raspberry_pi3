.globl _start
.globl _cpu0_exec
.globl _cpu1_exec
.globl _cpu2_exec
.globl _cpu3_exec

_start:
    b skip

skip:
    b master

master:
    mrc p15,0,r0,c0,c0,5
    ands r0, #0x3
    cmp r0, #0x00
    beq _cpu0_exec
    cmp r0, #0x01
    beq _cpu1_exec
    cmp r0, #0x02
    beq _cpu2_exec
    cmp r0, #0x03
    beq _cpu3_exec
loop:
    b loop

_cpu0_exec:
    ldr r0,=0x08000000
    mov sp, r0
    bl _start_code_cpu_0

_cpu1_exec:
    ldr r0,=0x08100000
    mov sp, r0
    bl _start_code_cpu_1

_cpu2_exec:
    ldr r0,=0x08200000
    mov sp, r0
    bl _start_code_cpu_2

_cpu3_exec:
    ldr r0,=0x08300000
    mov sp, r0
    bl _start_code_cpu_3


