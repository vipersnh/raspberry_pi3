#define BCM2837_PERIPHERAL_BASE 0x3F000000

#define BCM2837_GPIO_BASE_OFFSET    (0x200000)
#define BCM2837_GPIO_GPFSEL1_OFFSET     (0x04)
#define BCM2837_GPIO_GPSET0_OFFSET      (0x1C)
#define BCM2837_GPIO_GPCLR0_OFFSET      (0x28)

#define GPIO_PIN_16_F_SEL_OFFSET        18
#define GPIO_PIN_16_F_SEL_BITS          (0x07<<GPIO_PIN_16_F_SEL_OFFSET)
#define GPIO_PIN_OUTPUT_F_SEL           (0x01)
#define GPIO_PIN_INPUT_F_SEL            (0x00)

#define GPIO_PIN_16_SET_CLR             (0x1 << 16)

#define DELAY_1S_COUNT 699050   /* Calculated by trail and error */

#define CURR_DELAY (DELAY_1S_COUNT/4)

void dummy(unsigned int x) {
    x++;
}

int main(int cpu_id, char **ptr)
{
    unsigned int * gpio_sel_ptr, *gpio_set_ptr, *gpio_clr_ptr;
    unsigned long counter_val;

    gpio_sel_ptr = (unsigned int *)(BCM2837_PERIPHERAL_BASE + BCM2837_GPIO_BASE_OFFSET + BCM2837_GPIO_GPFSEL1_OFFSET);
    gpio_set_ptr = (unsigned int *)(BCM2837_PERIPHERAL_BASE + BCM2837_GPIO_BASE_OFFSET + BCM2837_GPIO_GPSET0_OFFSET);
    gpio_clr_ptr = (unsigned int *)(BCM2837_PERIPHERAL_BASE + BCM2837_GPIO_BASE_OFFSET + BCM2837_GPIO_GPCLR0_OFFSET);
 
    *gpio_sel_ptr = *gpio_sel_ptr & ~(GPIO_PIN_16_F_SEL_BITS);
    *gpio_sel_ptr |= GPIO_PIN_OUTPUT_F_SEL << GPIO_PIN_16_F_SEL_OFFSET;

    while(1)
    {
        *gpio_set_ptr |= GPIO_PIN_16_SET_CLR;
        for(counter_val=0;counter_val<CURR_DELAY;counter_val++) {
        };
        *gpio_clr_ptr |= GPIO_PIN_16_SET_CLR;
        for(counter_val=0;counter_val<CURR_DELAY;counter_val++) {
        };
    }
    return 0;
}

