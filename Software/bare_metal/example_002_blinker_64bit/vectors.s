.globl _start
.globl _cpu0_exec
.globl _cpu1_exec
.globl _cpu2_exec
.globl _cpu3_exec

_start:
    b master

.space 0x200000-0x0004,0

master:
    mrs x0, mpidr_el1
    ands x0, x0, #0x3
    cmp x0, #0x00
    beq _cpu0_exec
    cmp x0, #0x01
    beq _cpu1_exec
    cmp x0, #0x02
    beq _cpu2_exec
    cmp x0, #0x03
    beq _cpu3_exec
loop:
    b loop

_cpu0_exec:
    ldr x0,=0x08000000
    mov sp, x0
    bl _start_code_cpu_0

_cpu1_exec:
    ldr x0,=0x08100000
    mov sp, x0
    bl _start_code_cpu_1

_cpu2_exec:
    ldr x0,=0x08200000
    mov sp, x0
    bl _start_code_cpu_2

_cpu3_exec:
    ldr x0,=0x08300000
    mov sp, x0
    bl _start_code_cpu_3

